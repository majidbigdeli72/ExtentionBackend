﻿using Extention.Dto;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using PersonService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Channels;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;
using TicketService;

namespace ExtentionBackend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExtentionController : ControllerBase
    {
        private readonly ILogger<ExtentionController> _logger;
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;
        private readonly string _identitycode;
        private readonly string _ticketcode;
        private readonly string _persionCategory;


        public ExtentionController(
            ILogger<ExtentionController> logger,
            IWebHostEnvironment env,
            IConfiguration iConfig
            )
        {
            _logger = logger;
            _env = env;
            _config = iConfig;
            _identitycode = _config.GetValue<string>("IdentityCode");
            _ticketcode = _config.GetValue<string>("TicketCode");
            _persionCategory = _config.GetValue<string>("PersonCategory");

        }


        [HttpPost("SaveTicket")]
        public async Task<string> SaveTicketAsync(InputDto input)
        {
            try
            {
                var query = $"email == \"{input.Email}\"";
                Guid identityId;

                var personService = new PayamgostarServiceClient<IPerson>(input.EndPointAddress, input.IsHttps).Create();

                var identityPerson = await personService.SearchPersonAsync(input.UserName, input.Password, _identitycode, query);

                if (identityPerson.PersonInfoList.Count() == default)
                {
                    identityId = await CreatePersonAsync(input, personService);
                }
                else
                {
                    identityId = identityPerson.PersonInfoList[0].CrmId.Value;
                }

                var ticketQuery = $"SourceRefId == \"{input.RefId}\" && Title == \"{input.Title}\"";

                var ticketService = new PayamgostarServiceClient<ITicket>(input.EndPointAddress, input.IsHttps).Create();

                var ticket = await ticketService.SearchTicketAsync(input.UserName, input.Password, _ticketcode, ticketQuery);

                if (ticket.TicketInfoList.Count() == default)
                {
                    var result = await SaveTicket(input, identityId, ticketService);
                    if (result.Success)
                    {
                        return $"هویت {input.FullName} با موفقیت ثبت شد.";
                    }
                    else
                    {
                        return result.Message;
                    }
                }
                else
                {
                    return $"هویت {input.FullName} با موفقیت ثبت شد.";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error In SaveTicket");
                throw ex;
            }


        }

        private async Task<TicketService.SaveCrmObjectResult> SaveTicket(InputDto input, Guid identityId, ITicket ticketService)
        {
            ResonseFileDto resonseUploadFile;

            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(input.UserName, input.Password);
                string fileName = await DownloadFileAsync(input.FileAddress, input.FullName, client);
                resonseUploadFile = await UploadFileAsync(client, fileName, input.EndPointAddress);
                System.IO.File.Delete(fileName);

            }



            var baseCrmObjectExtendedPropertyInfo = new List<TicketService.BaseCrmObjectExtendedPropertyInfo>(){
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "Important",Name="Important",Value =input.Important.ToString() },
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "Source",Name="Source",Value =input.Source },
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "Title",Name="Title",Value =input.Title },
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "ApproverNote",Name="ApproverNote",Value =input.ApproverNote },
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "Approver",Name="Approver",Value =input.Approver },
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "SourceRefId",Name="SourceRefId",Value =input.RefId },
                        new TicketService.BaseCrmObjectExtendedPropertyInfo(){ UserKey = "ResumeFile",Name="ResumeFile",Value = resonseUploadFile.FileId.ToString() }
                    };

            var saveCrmObjectResult = await ticketService.SaveticketAsync(input.UserName, input.Password, new TicketInfo()
            {
                IdentityId = identityId,
                CrmObjectTypeCode = _ticketcode,
                Subject = input.Title,
                EmailAddress = input.Email,
                ExtendedProperties = baseCrmObjectExtendedPropertyInfo.ToArray(),
            });

            return saveCrmObjectResult;
        }

        private async Task<Guid> CreatePersonAsync(InputDto input, IPerson personService)
        {
            var phoneContacts = new List<IdentityContactPhone> { new IdentityContactPhone { PhoneType = "موبایل", PhoneNumber = input.Phone, IsDefault = true } };
            var categories = new List<CategoryInfo> { new CategoryInfo { Key = _persionCategory } };
            var saveCrmObjectResult = await personService.SavePersonAsync(input.UserName, input.Password, new PersonInfo()
            {
                Emails = new string[] { input.Email },
                LastName = input.FullName,
                PhoneContacts = phoneContacts.ToArray(),
                Categories = categories.ToArray(),
                IdentityType = "حقیقی",
                CrmObjectTypeCode = _identitycode
            });
            return saveCrmObjectResult.CrmId;
        }

        private async Task<ResonseFileDto> UploadFileAsync(WebClient client, string fileName, string baseEndPoint)
        {
            var endpoint = $"{baseEndPoint}{_config.GetValue<string>("FileUploadUrl")}";
            Uri fileUploadUrl = new Uri(endpoint);
            byte[] responseArray = await client.UploadFileTaskAsync(fileUploadUrl, fileName);
            return JsonSerializer.Deserialize<ResonseFileDto>(responseArray);
        }

        private async Task<string> DownloadFileAsync(string address, string name, WebClient client)
        {
            string webRootPath = _env.ContentRootPath;
            Uri fileAddress = new Uri(address);
            // string fname = HttpUtility.ParseQueryString(fileAddress.Query).Get("download_uri");
            // string file = Path.GetFileNameWithoutExtension(fname);
            // string newName = fname.Replace(file, name);
            var fileName = Path.Combine(webRootPath, "Resume", name + ".pdf");
            await client.DownloadFileTaskAsync(fileAddress, fileName);
            return fileName;
        }
    }
}