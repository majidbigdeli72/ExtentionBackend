﻿using PersonService;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketService;

namespace ExtentionBackend.Controllers
{
    public class PayamgostarServiceClient<TChannel>
    {
        private readonly string _endPointAddress;
        private readonly bool _isHttps;

        public PayamgostarServiceClient(string endPointAddress,bool isHttps)
        {
            _endPointAddress = endPointAddress;
            _isHttps = isHttps;
        }

        public TChannel Create()
        {
            string uri = $"{_endPointAddress}{ServicePathLocation}";
            return new PayamgostarServiceClientFactory<TChannel>(uri, _isHttps).CreateChannel();
        }

        private string ServicePathLocation
        {
            get
            {
                return new Dictionary<Type, string>
                {
                    { typeof(IPerson), "/Services/API/IPerson.svc"},
                    { typeof(ITicket), "/Services/API/ITicket.svc"},
                }.First(x => x.Key == typeof(TChannel)).Value;
            }
        }
    }
}