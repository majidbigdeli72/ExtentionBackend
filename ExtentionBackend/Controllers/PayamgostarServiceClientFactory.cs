﻿using System.ServiceModel;

namespace ExtentionBackend.Controllers
{
    public class PayamgostarServiceClientFactory<TChannel> : ChannelFactory<TChannel>
    {
        public PayamgostarServiceClientFactory(string endPointAddress,bool isHttps) : base(new BasicHttpBinding
        {
            Security = new BasicHttpSecurity() { Mode = isHttps ? BasicHttpSecurityMode.Transport : BasicHttpSecurityMode.None },
            MaxBufferPoolSize = int.MaxValue,
            MaxBufferSize = int.MaxValue,
            MaxReceivedMessageSize = int.MaxValue,
            ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas
            {
                MaxArrayLength = int.MaxValue,
                MaxBytesPerRead = int.MaxValue,
                MaxDepth = int.MaxValue,
                MaxNameTableCharCount = int.MaxValue,
                MaxStringContentLength = int.MaxValue
            }
        }, new EndpointAddress(endPointAddress))
        {
        }
    }
}