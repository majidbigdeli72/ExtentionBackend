﻿namespace Extention.Dto
{
    public class InputDto
    {
        public string Email { get; set; }
        public string Phone { get; set; }
        public string FullName { get; set; }
        public string Approver { get; set; }
        public bool Important { get; set; }
        public string Source { get; set; }
        public string Title { get; set; }
        public string ApproverNote { get; set; }
        public string FileAddress { get; set; }
        public string RefId { get; set; }
        public string EndPointAddress { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsHttps { get; set; }
    }

}
