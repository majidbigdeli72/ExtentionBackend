﻿using System;

namespace Extention.Dto
{
    public class ResonseFileDto
    {
        public Guid FileId { get; set; }
        public string FileName { get; set; }
    }
}
